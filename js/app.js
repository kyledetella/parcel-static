const classes = {
  LOADING: "loading"
};

export const App = () => {
  const container = document.createElement("div");
  container.id = "app";
  const button = document.createElement("button");

  const loadingIndicator = document.createElement("div");
  loadingIndicator.className = "loadingIndicator";
  loadingIndicator.innerHTML = "Loading...";

  let state = { isLoading: false };

  const Update = () => {
    if (state.isLoading) {
      container.classList.add(classes.LOADING);
    } else {
      container.classList.remove(classes.LOADING);
    }
  };

  button.innerHTML = "Start";

  button.addEventListener("click", () => {
    console.log("Click");
    state.isLoading = true;

    setTimeout(() => {
      state.isLoading = false;
    }, 1000);
  });

  container.appendChild(button);
  container.appendChild(loadingIndicator);

  setInterval(() => {
    Update();
  }, 60);

  return container;
};
