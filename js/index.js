import { App } from "./app";

const render = (el, node) => node.appendChild(el);

render(App(), document.body);

if (process.env.NODE_ENV !== "production") {
  // TODO: Remove this in development mode!!!!
  // This has to be added since Parcel's HMR will simply add a new instance of your application
  // to the `index.html` instead of replacing the existing one. There seems to be contention in
  // the Parcel community on this topic.
  // https://github.com/parcel-bundler/parcel/issues/289
  // https://github.com/parcel-bundler/parcel/issues/344
  if (module.hot) {
    module.hot.dispose(() => {
      window.location.reload();
    });
  }
}
