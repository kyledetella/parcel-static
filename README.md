# parcel-static

Trying out [parcel](https://parceljs.org/) and deploying the result as a static site on S3.

Deployed here: http://parcel.static.s3-website.us-east-2.amazonaws.com/
And here: http://d158uag3chcmou.cloudfront.net